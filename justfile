_default:
    @just --list -u

partition := "icelake"
cores := if partition == "icelake" {"76"} else {if partition == "sapphire" {"112"} else {"22"}}
targets := ""
tmp := ""
tmpvar := if tmp == "" {""} else {"export TMPDIR=" + tmp + " && "}
jobs := "12"

sbatch *targets=targets:
    {{tmpvar}} nohup snakemake \
    --cores {{cores}} --jobs {{jobs}} \
    --software-deployment-method conda apptainer \
    --executor slurm --default-resources slurm_partition={{partition}} --default-resources runtime=720 \
    -k --rerun-incomplete --conda-frontend conda \
    {{targets}} &

scancel:
    scancel -u ${USER} && snakemake --unlock

sclean:
    rm slurm*.out nohup.out

activate:
    conda activate smk && snakemake -n | less -S

sclear: scancel && sclean
