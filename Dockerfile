FROM condaforge/mambaforge:latest
LABEL io.github.snakemake.containerized="true"
LABEL io.github.snakemake.conda_env_hash="3a16ea1b92334ca2c9d559bd07f3ba59b53eeb480baad05311071001b6c3f408"

# Step 1: Retrieve conda environments

# Conda environment:
#   source: https://github.com/snakemake/snakemake-wrappers/raw/v1.27.0/bio/bwa-mem2/index/environment.yaml
#   prefix: /conda-envs/54f5ec7d54dfd4f34e5612d68897cc0b
#   channels:
#     - conda-forge
#     - bioconda
#     - nodefaults
#   dependencies:
#     - bwa-mem2 =2.2.1
RUN mkdir -p /conda-envs/54f5ec7d54dfd4f34e5612d68897cc0b
ADD https://github.com/snakemake/snakemake-wrappers/raw/v1.27.0/bio/bwa-mem2/index/environment.yaml /conda-envs/54f5ec7d54dfd4f34e5612d68897cc0b/environment.yaml

# Conda environment:
#   source: workflow/envs/bwa-mem2.yaml
#   prefix: /conda-envs/3a012cb8df7aa2725ed231709488a57a
#   channels:
#     - conda-forge
#     - bioconda
#     - defaults
#   dependencies:
#     - bwa-mem2==2.2.1
#     - samtools==1.16.1
RUN mkdir -p /conda-envs/3a012cb8df7aa2725ed231709488a57a
COPY workflow/envs/bwa-mem2.yaml /conda-envs/3a012cb8df7aa2725ed231709488a57a/environment.yaml

# Conda environment:
#   source: workflow/envs/curl.yaml
#   prefix: /conda-envs/df9e388b7d819deab94210d249644ea1
#   channels:
#     - conda-forge
#     - bioconda
#   dependencies:
#     - curl>7
#     - samtools>1
RUN mkdir -p /conda-envs/df9e388b7d819deab94210d249644ea1
COPY workflow/envs/curl.yaml /conda-envs/df9e388b7d819deab94210d249644ea1/environment.yaml

# Conda environment:
#   source: workflow/envs/fastqc.yaml
#   prefix: /conda-envs/59452208320d1fe94cdfbb527acd7898
#   name: fastqc
#   channels:
#     - bioconda
#     - conda-forge
#   dependencies:
#     - fastqc=0.11.9
RUN mkdir -p /conda-envs/59452208320d1fe94cdfbb527acd7898
COPY workflow/envs/fastqc.yaml /conda-envs/59452208320d1fe94cdfbb527acd7898/environment.yaml

# Conda environment:
#   source: workflow/envs/multiqc.yaml
#   prefix: /conda-envs/afd839e2024b79e8c038c3c092d369bb
#   channels:
#     - conda-forge
#     - bioconda
#     - defaults
#   dependencies:
#     - multiqc>=1.10
RUN mkdir -p /conda-envs/afd839e2024b79e8c038c3c092d369bb
COPY workflow/envs/multiqc.yaml /conda-envs/afd839e2024b79e8c038c3c092d369bb/environment.yaml

# Conda environment:
#   source: workflow/envs/samtools.yaml
#   prefix: /conda-envs/6ed692802028e37bc88c92088fdf471c
#   channels:
#     - conda-forge
#     - bioconda
#   dependencies:
#     - samtools=1.16.1
RUN mkdir -p /conda-envs/6ed692802028e37bc88c92088fdf471c
COPY workflow/envs/samtools.yaml /conda-envs/6ed692802028e37bc88c92088fdf471c/environment.yaml

# Conda environment:
#   source: workflow/envs/tidyverse-R.yaml
#   prefix: /conda-envs/5eec217b4a38d319de18df4edd97f8d6
#   name: tidyverse-R
#   channels:
#     - conda-forge
#     - bioconda
#     - defaults
#   dependencies:
#     - r-base==4.2.3
#     - r-tidyverse==1.3.2
#     - r-cairo=1.6
RUN mkdir -p /conda-envs/5eec217b4a38d319de18df4edd97f8d6
COPY workflow/envs/tidyverse-R.yaml /conda-envs/5eec217b4a38d319de18df4edd97f8d6/environment.yaml

# Step 2: Generate conda environments

RUN mamba env create --prefix /conda-envs/54f5ec7d54dfd4f34e5612d68897cc0b --file /conda-envs/54f5ec7d54dfd4f34e5612d68897cc0b/environment.yaml && \
    mamba env create --prefix /conda-envs/3a012cb8df7aa2725ed231709488a57a --file /conda-envs/3a012cb8df7aa2725ed231709488a57a/environment.yaml && \
    mamba env create --prefix /conda-envs/df9e388b7d819deab94210d249644ea1 --file /conda-envs/df9e388b7d819deab94210d249644ea1/environment.yaml && \
    mamba env create --prefix /conda-envs/59452208320d1fe94cdfbb527acd7898 --file /conda-envs/59452208320d1fe94cdfbb527acd7898/environment.yaml && \
    mamba env create --prefix /conda-envs/afd839e2024b79e8c038c3c092d369bb --file /conda-envs/afd839e2024b79e8c038c3c092d369bb/environment.yaml && \
    mamba env create --prefix /conda-envs/6ed692802028e37bc88c92088fdf471c --file /conda-envs/6ed692802028e37bc88c92088fdf471c/environment.yaml && \
    mamba env create --prefix /conda-envs/5eec217b4a38d319de18df4edd97f8d6 --file /conda-envs/5eec217b4a38d319de18df4edd97f8d6/environment.yaml && \
    mamba clean --all -y
