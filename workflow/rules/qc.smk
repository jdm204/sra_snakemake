rule compare_coverage:
    input: expand("results/qc/{patient_sample}_{ref}_coverage.tsv",\
                  patient_sample=PATIENT_SAMPLES,\
                  ref=config["reference_genome"]),
    output:
        tsv="results/qc/coverage.tsv",
        plot="results/qc/coverage.svg",
    conda: "../envs/tidyverse-R.yaml",
    script: "../scripts/coverage.R"

rule coverage:
    input: "results/alignment/{patient}_{sample}_{ref}.cram",
    output: "results/qc/{patient}_{sample}_{ref}_coverage.tsv",
    log: "logs/qc/{patient}_{sample}_{ref}_coverage.log",
    conda: "../envs/samtools.yaml",
    shell:
        """
        (samtools coverage \
        {input} -o {output} \
        ) &> {log}
        """

rule fastqc:
    input: pair_path,
    output:
        html="results/qc/{patient}_{sample}_{unit}_{pair}.html",
        zip="results/qc/{patient}_{sample}_{unit}_{pair}_fastqc.zip",
    params:
        cmdline="--quiet",
        outdir="results/qc/{patient}_{sample}_{unit}_{pair}/",
    log: "logs/qc/{patient}_{sample}_{unit}_{pair}.log",
    threads: 2
    conda: "../envs/fastqc.yaml",
    shell:
        """
        (mkdir -p {params.outdir} && \
        fastqc {params.cmdline} -t {threads} \
        --outdir {params.outdir} {input} \
        && mv {params.outdir}*.html {output.html} \
        && mv {params.outdir}*.zip {output.zip} \
        && rmdir {params.outdir}) &> {log}
        """

rule multiqc:
    input: lambda wc: expand("results/qc/{file}_fastqc.zip", file=FILES),
    output: "results/qc/multiqc_report.html",
    log: "logs/qc/multiqc.log",
    conda: "../envs/multiqc.yaml",
    shell:
        """
        multiqc \
        $(dirname {input[0]}) -o $(dirname {output}) \
        --no-data-dir \
        &> {log}
        """
