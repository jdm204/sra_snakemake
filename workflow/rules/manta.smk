rule manta_tumour_normal:
    input:
        fasta="resources/ref/{ref}/{ref}.fa",
        fai="resources/ref/{ref}/{ref}.fa.fai",
        tumour_cram="results/alignment/{patient}_{tsample}_{ref}.cram",
        tumour_cramidx="results/alignment/{patient}_{tsample}_{ref}.cram.crai",
        normal_crams=patient_normal_crams,
    params:
        outdir="results/variants/manta/{patient}_{tsample}_{ref}/",
        normal_str=lambda wc, input: [f"--normalBam {cram} " for cram in input.normal_crams],
    output: directory("results/variants/manta/{patient}_{tsample}_{ref}"),
    log: "logs/manta/{patient}_{tsample}_{ref}.log",
    conda: "../envs/manta.yaml",
    threads: 24,
    shell:
        """
        (
        configManta.py \
        --runDir {params.outdir} \
        --reference {input.fasta} \
        {params.normal_str} \
        --tumorBam {input.tumour_cram} \
        && \
        cd {params.outdir} \
        && \
        ./runWorkflow.py \
        -m local -j {threads}
        ) &> {log}
        """
