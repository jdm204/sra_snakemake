# TODO: allow tuning width param in config
rule sequenza:
    input: "results/variants/sequenza/{patient}_{tumour_sample}_{ref}_binned.seqz.gz",
    output: directory("results/variants/sequenza/{patient}_{tumour_sample}_{ref}/"),
    conda: "../envs/sequenza.yaml",
    script: "../scripts/sequenza.R"

rule sequenza_binning:
    input: "results/variants/sequenza/{patient}_{tumour_sample}_{ref}.seqz.gz",
    output: temp("results/variants/sequenza/{patient}_{tumour_sample}_{ref}_binned.seqz.gz"),
    conda: "../envs/sequenza-utils.yaml",
    params: width="50",
    shell: "sequenza-utils seqz_binning --seqz {input} -w {params.width} -o {output}"

rule sequenza_seqz:
    input:
        normal_pileup=lambda wc: f"results/pileup/{wc.patient}_{patient_normal_samples(wc)[0]}_{wc.ref}.pileup",
        tumour_pileup="results/pileup/{patient}_{tumour_sample}_{ref}.pileup",
        ref="resources/ref/{ref}/{ref}.fa.gz",
        wiggle="resources/ref/{ref}/gc_50_base.wig.gz",
    output: temp("results/variants/sequenza/{patient}_{tumour_sample}_{ref}.seqz.gz"),
    conda: "../envs/sequenza-utils.yaml",
    shell:
        """
        sequenza-utils bam2seqz \
        -n {input.normal_pileup} -t {input.tumour_pileup} \
        --fasta {input.ref} \
        -gc {input.wiggle} \
        -o {output}
        """

rule sequenza_wiggle:
    input: "resources/ref/{ref}/{ref}.fa.gz",
    output: temp("resources/ref/{ref}/gc_50_base.wig.gz"),
    conda: "../envs/sequenza-utils.yaml",
    params: width=50,
    shell:
        """
        sequenza-utils gc_wiggle \
        -w {params.width} \
        --fasta {input} \
        -o {output}
        """
