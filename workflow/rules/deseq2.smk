rule deseq_comparison:
    input:
        dds="results/deseq2/{formula}_{ref}.rds",
        gene_info="resources/ref/hg38/gene_info.rds",
        sample_table=config["units"],
    output:
        results_table="results/deseq2/{formula}@{comparison}_{ref}.tsv.gz",
    params:
        design="{formula}",
        comparison="{comparison}",
        ref="{ref}"
    threads: 8,
    resources:
        mem_mb=10000,
    log: "logs/deseq2/{formula}@{comparison}_{ref}.log",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/differential_expression.R"

rule gene_info:
    output:
        gene_info="resources/ref/hg38/gene_info.rds",
    threads: 8,
    resources:
        mem_mb=10000,
    conda: "../envs/deseq2.yaml",
    log: "logs/deseq2/gene_info.log",
    script: "../scripts/gene_info.R"

rule deseq_tximport_hg38noncode:
    input:
        genes="results/tximport/genes_hg38-noncodev6-genscriptome.rds",
        sample_table=config["units"],
    output:
        deseq="results/deseq2/{formula}_hg38-noncodev6-genscriptome.rds",
    params:
        design="{formula}",
        ref="hg38-noncodev6-genscriptome",
    threads: 8,
    resources:
        mem_mb=10000,
    log: "logs/deseq2/{formula}_hg38-noncodev6-genscriptome.log",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/deseq_import.R"

rule deseq_tximport_hg38:
    input:
        genes="results/tximport/genes_hg38.rds",
        sample_table=config["units"],
    output:
        deseq="results/deseq2/{formula}_hg38.rds",
    params:
        design="{formula}",
        ref="hg38",
    threads: 8,
    resources:
        mem_mb=10000,
    log: "logs/deseq2/{formula}_hg38.log",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/deseq_import_tximeta.R"

# rule transcripts_qc:
#     input:
#         txs="results/tximport/transcripts_{ref}.rds",
#         genes="results/tximport/genes_{ref}.rds",
#     output: directory("results/tximport/transcripts_qc"),
#     conda: "../envs/deseq2.yaml",
#     params: "",
#     script: "../scripts/transcripts_qc.R"

rule tximeta_hg38:
    input:
        quants=expand("results/salmon/{patient_sample}_hg38/",
                      patient_sample=PATIENT_SAMPLES),
        sample_table=config["units"],
    output:
        txs="results/tximport/transcripts_hg38.rds",
        genes="results/tximport/genes_hg38.rds",
    params:
        ref="hg38",
        q_prefix="results/salmon",
        q_suffix="quant.sf",
    threads: 8,
    resources:
        mem_mb=10000,
    log: "logs/tximeta/hg38.log",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/tximeta.R"

rule tximport_hg38noncode:
    input:
        quants=expand("results/salmon/{patient_sample}_hg38-noncodev6-genscriptome/",
                      patient_sample=PATIENT_SAMPLES),
        sample_table=config["units"],
        tx2gene="resources/ref/hg38-noncodev6-genscriptome/tx2gene.tsv.gz",
    output:
        txs="results/tximport/transcripts_hg38-noncodev6-genscriptome.rds",
        genes="results/tximport/genes_hg38-noncodev6-genscriptome.rds",
    params:
        ref="hg38-noncodev6-genscriptome",
        q_prefix="results/salmon",
        q_suffix="quant.sf",
    threads: 8,
    resources:
        mem_mb=10000,
    log: "logs/tximport/hg38-noncodev6-genscriptome.log",
    conda: "../envs/deseq2.yaml",
    script: "../scripts/tximport.R"
