rule getfastq:
    output:
        "data/fastq/{accession}_1.fastq.gz",
        "data/fastq/{accession}_2.fastq.gz",
    log:
        "logs/fastq/{accession}.gz.log"
    params:
        extra="--skip-technical -f -t temp"
    threads: 6  # defaults to 6
    wrapper:
        "v0.85.1/bio/sra-tools/fasterq-dump"
