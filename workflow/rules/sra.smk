rule retrieve_sra_run:
    output: expand("input/{{db_accession}}_{read}.fastq.gz", read=[1,2]),
    conda: "../envs/ffq.yaml",
    log: "logs/retrieve_sra_run/{db_accession}.log",
    retries: 3,
    shell:
        """
        (
        cd input && \
        ffq --ftp {wildcards.db_accession} \
        | grep -Eo '"url": "[^"]*"' \
        | grep -o '"[^"]*"$' \
        | xargs curl --remote-name-all \
        ) &> {log}
        """
