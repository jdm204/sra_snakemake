RELEASE = "109"
ENSEMBL_HUMAN = f"ftp://ftp.ensembl.org/pub/release-{RELEASE}/fasta/homo_sapiens"
ENSEMBL_MOUSE = f"ftp://ftp.ensembl.org/pub/release-{RELEASE}/fasta/mus_musculus"

TOPLEVEL_HUMAN = "Homo_sapiens.GRCh38.dna.toplevel.fa.gz"
TOPLEVEL_HUMAN_DOWNLOAD = f"{ENSEMBL_HUMAN}/dna/{TOPLEVEL_HUMAN}"

TOPLEVEL_MOUSE = "Mus_musculus.GRCm39.dna.toplevel.fa.gz"
TOPLEVEL_MOUSE_DOWNLOAD = f"{ENSEMBL_MOUSE}/dna/{TOPLEVEL_MOUSE}"

rule count_species_reads:
    input: expand("results/xengsort/{{patient}}_{{sample}}_{{unit}}-k{{k}}-{species}.1.fq", species=[]),
    output: "xengsort/species.tsv",
    shell: "wc -l {input}/* > {output} "

rule compress_xengsort_reads:
    input: "results/xengsort/{patient}_{sample}_{unit}-k{k}-graft.{read}.fq",
    output: "results/xengsort/{patient}_{sample}_{unit}-k{k}-graft.{read}.fq.gz",
    shell: "gzip {input} "

rule get_xengsort:
    output: directory("resources/xengsort/"),
    params: "https://gitlab.com/genomeinformatics/xengsort",
    conda: "../envs/xengsort.yaml",
    shell: "mkdir -p {output} && git clone {params} {output} && cd {output} && python setup.py develop"

rule xengsort_classify:
    input:
        xengsort="resources/xengsort/",
        index="resources/xengsort_data/xengsort-k{k}.zarr",
        reads=sample_unit_paths,
    output:
        R1=temp("results/xengsort/{patient}_{sample}_{unit}-k{k}-graft.1.fq"),
        R2=temp("results/xengsort/{patient}_{sample}_{unit}-k{k}-graft.2.fq"),
    log: "logs/xengsort_classify/{patient}_{sample}_{unit}-k{k}.log",
    params:
        outprefix=lambda wc: f"results/xengsort/{wc.patient}_{wc.sample}_{wc.unit}-k{wc.k}",
        chunksize=16.0,  # MB per chunk per thread
        prefetch=1,
        debug="-DD",  # debug level 2
    conda: "../envs/xengsort.yaml"        
    threads: 8
    shell:
        """
        (
         xengsort {params.debug} classify --out {params.outprefix} --index {input.index} \
         --threads {threads} --fastq <(zcat {input.reads[0]}) --pairs <(zcat {input.reads[1]}) \
         --chunksize {params.chunksize} --prefetch {params.prefetch} \
        ) &> {log}
        """
        
# A rule that demonstrates how to build an index with xengsort
# Note that you need to give an estimate of the number of k-mers in all input files.
# For human and mouse DNA and cDNA, and k=25, this is approx. 4.5 billion,
# so this number is given as size parameter below.
rule xengsort_index:
    input:
        xengsort="resources/xengsort/",
        dna_human=f"resources/xengsort_data/{TOPLEVEL_HUMAN}",
        dna_mouse=f"resources/xengsort_data/{TOPLEVEL_MOUSE}",
    output: directory("resources/xengsort_data/xengsort-k{k}.zarr"),
    log: "logs/xengsort_index/xengsort-k{k}.log",
    params:
        size=4496607845,  # size only correct for k=25!
        fill=0.88,  # total space in table is size/fill
        subtables=9,
        weakthreads=11,
        bucketsize=4,
        shortcutbits=0,
        debug="-DD",  # debug level 2
    conda: "../envs/xengsort.yaml"
    threads: 11
    shell:
        """
        (
        xengsort {params.debug} index --index {output} \
         -G <(zcat {input.dna_human}) \
         -H <(zcat {input.dna_mouse}) \
         -k {wildcards.k} -n {params.size} \
         -p {params.bucketsize} --fill {params.fill} -W {params.weakthreads} \
         --shortcutbits {params.shortcutbits} \
        ) &> {log}
        """
        
rule download_refs:
    output:
        f"resources/xengsort_data/{TOPLEVEL_HUMAN}",
        f"resources/xengsort_data/{TOPLEVEL_MOUSE}",
    shell:
        """
        wget {TOPLEVEL_HUMAN_DOWNLOAD} -P resources/xengsort_data/
        wget {TOPLEVEL_MOUSE_DOWNLOAD} -P resources/xengsort_data/
        """
