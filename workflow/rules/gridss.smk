rule gridss:
    input:
        normal_crams=patient_normal_crams,
        tumour_crams=patient_tumour_crams,
        ref="resources/ref/{ref}/{ref}.fa",
        refindex="resources/ref/{ref}/{ref}.fa.bwt",
    output: "results/variants/gridss/{patient}_{ref}.vcf.gz",
    conda: "../envs/gridss.yaml",
    params:
        blacklist="--blacklist <exclude_list.bed> ",
        coverage="--maxcoverage 50000 ",
        workdir="temp/gridss",
    log: "logs/gridss/{patient}_{ref}.log",
    shell:
        """
        gridss \
        --reference {input.ref} \
        --output {output} \
        --threads {threads} \
        --workingdir {params.workdir} \
        --steps preprocess,assemble,call \
        {input.normal_crams} {input.tumour_crams} \
        &> {log}
        """

rule gridss_setup_reference:
    input:
        ref="resources/ref/{ref}/{ref}.fa",
    output: "resources/ref/{ref}/{ref}.fa.bwt",
    conda: "../envs/gridss.yaml",
    params:
        java_opts="--jvmheap 3g ",
    shell:
        """
        gridss \
        --reference {input.ref} \
        --threads {threads} \
        {params.java_opts} \
        --workingdir temp/gridss \
        --steps setupreference
        """
















