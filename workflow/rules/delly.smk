rule vep_table_delly:
    input: "results/variants/delly/filtered/{patient}_hg38.bcf",
    output: "results/variants/delly/filtered/{patient}_hg38.tsv.gz",
    conda: "../envs/bcftools.yaml",
    params:
        cols='%CHROM\\t%POS\\t%ID\\t%REF\\t%ALT\\t%QUAL\\t%FILTER',
        info='\\t%CIEND\\t%CIPOS\\t%CHR2\\t%POS2\\t%END\\t%PE\\t%MAPQ\\t%SRMAPQ\\t%SR\\t%SRQ\\t%CONSENSUS\\t%CE\\t%CT\\t%SVLEN\\t%IMPRECISE\\t%PRECISE\\t%SVTYPE\\t%SVMETHOD\\t%INSLEN\\t%HOMLEN',
        format='\\t%GT\\t%GL\\t%GQ\\t%FT\\t%RC\\t%RCL\\t%RCR\\t%RDCN\\t%DR\\t%DV\\t%RR\\t%RV',
    shell: "bcftools query -H -f '{params.cols}{params.info}[{params.format}]\\n' {input} | gzip > {output} "

rule filter_delly:
    input:
        bcf="results/variants/delly/{patient}_hg38.bcf",
        sample_table="resources/delly/sample_table_{patient}.tsv",
    output: "results/variants/delly/filtered/{patient}_hg38.bcf",
    conda: "../envs/delly.yaml",
    shell:
        """
        delly filter -f somatic -o {output} -s {input.sample_table} {input.bcf}
        """

rule make_sample_table:
    output: "resources/delly/sample_table_{patient}.tsv",
    params:
        normal=patient_normal_samples,
        tumours=patient_tumour_samples,
    run:
        with open(output[0], "w") as out:
            out.write(f"{params.normal[0]}\tcontrol\n")
            for tumour in params.tumours:
                out.write(f"{tumour}\ttumor\n")

rule delly:
    input:
        ref="resources/ref/{ref}/{ref}.fa.gz",
        crams=patient_sample_crams,
        hg38_exclude = "resources/hg38.excludes",
    output: "results/variants/delly/{patient}_{ref}.bcf",
    conda: "../envs/delly.yaml",
    params: "-q 20"
    shell:
        """
        delly call \
        {params} \
        -x {input.hg38_exclude} \
        -o {output} \
        -g {input.ref} \
        {input.crams}
        """

rule get_hg38_excludes_for_delly:
    output: "resources/hg38.excludes",
    params: "https://raw.githubusercontent.com/dellytools/delly/master/excludeTemplates/human.hg38.excl.tsv",
    shell: "wget {params} -O {output} "
