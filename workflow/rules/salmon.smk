rule salmon_mapping:
    input:
        read1s=lambda wc: reads_for_salmon(wc, 1),
        read2s=lambda wc: reads_for_salmon(wc, 2),
        index="resources/ref/{ref}/salmon_sa_index/",
    output: directory("results/salmon/{patient}_{sample}_{ref}"),
    conda: "../envs/salmon.yaml",
    params:
        main="-l A --validateMappings --gcBias --seqBias",
    threads: 32,
    resources:
        mem_mb=100000,
    log: "logs/salmon_mapping/{patient}_{sample}_{ref}.log",
    shell:
        """
        (
        salmon quant \
        -p {threads} \
        {params.main} \
        -i {input.index} \
        -1 {input.read1s} -2 {input.read2s} \
        -o {output} \
        ) &> {log}
        """

rule salmon_index_mlrna:
    input:
        decoys="resources/ref/hg38-noncodev6/decoys.txt",
        fasta="resources/ref/hg38-noncodev6-genscriptome/hg38-noncodev6-genscriptome.fa.gz",
    output: directory("resources/ref/hg38-noncodev6-genscriptome/salmon_sa_index/"),
    conda: "../envs/salmon.yaml",
    resources:
        mem_mb=100000,
    threads: 32,
    log: "logs/salmon_index/hg38-noncodev6-genscriptome.log",
    shell:
        """
        (
        salmon index -p {threads} -t {input.fasta} -i {output} --decoys {input.decoys} -k 31
        ) &> {log}
        """

rule salmon_hg38_noncode_genscriptome:
    input:
        transcriptome="resources/ref/hg38-transcriptome/hg38-transcriptome.fa.gz",
        noncode="resources/ref/noncodev6/noncodev6.fa.gz",
        genome="resources/ref/hg38/hg38.fa.gz",
    output: "resources/ref/hg38-noncodev6-genscriptome/hg38-noncodev6-genscriptome.fa.gz",
    conda: "../envs/curl.yaml",
    shell:
        """
        bgzip -dc {input.transcriptome} {input.noncode} {input.genome} | bgzip -c > {output}
        """

rule salmon_decoys_file:
    input: "resources/ref/hg38/hg38.fa.gz",
    output: "resources/ref/hg38-noncodev6/decoys.txt",
    conda: "../envs/curl.yaml",
    shell:
        """
        grep "^>" <(bgzip -dc {input}) | cut -d " " -f 1 > {output} && \
        sed -i.bak -e 's/>//g' {output}
        """

