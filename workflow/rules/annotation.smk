rule vcf2maf_hg38:
    input:
        ref="resources/ref/hg38/hg38.fa.gz",
        vcf="results/variants/{caller}/filtered/{patient}_hg38.vcf",
        vep_data="resources/ref/hg38/ensembl-vep-data",
    output: temp("results/variants/{caller}/annotated/{patient}_{tsample}_hg38.maf"),
    log: "logs/vcf2maf/{caller}/{patient}_{tsample}_hg38.log",
    conda: "../envs/vcf2maf.yaml",
    params:
        tstring=lambda wc: f"--tumor-id {wc.tsample} ",
        nstring=lambda wc: [f"--normal-id {n} " for n in patient_normal_samples(wc)],
    shell:
        """
        vcf2maf.pl \
        --input-vcf {input.vcf} \
        --output-maf {output} \
        --ref-fasta {input.ref} \
        {params.tstring} \
        {params.nstring} \
        --vep-data {input.vep_data} \
        --vep-path $(dirname $(which vep)) \
        --ncbi-build GRCh38 \
        --vep-overwrite \
        &> {log}
        """

rule decompress_filtered_vcf:
    input: "results/variants/{caller}/filtered/{patient}_{ref}.vcf.gz",
    output: temp("results/variants/{caller}/filtered/{patient}_{ref}.vcf"),
    conda: "../envs/samtools.yaml",
    shell:
        """
        bgzip -dc {input} > {output}
        """

rule bgzip_maf:
    input: "results/variants/{caller}/annotated/{patient}_{tsample}_{ref}.maf",
    output: "results/variants/{caller}/annotated/{patient}_{tsample}_{ref}.maf.gz",
    log: "logs/bgzip_maf/{caller}/{patient}_{tsample}_{ref}.log",
    conda: "../envs/samtools.yaml",
    shell:
        """
        bgzip -c {input} > {output} 2> {log}
        """

rule normalise_small_variants:
    input:
        ref="resources/ref/{ref}/{ref}.fa.gz",
        vcf="results/variants/{caller}/filtered/{patient}_{ref}.vcf.gz",
    output: "results/variants/{caller}/normalised/{patient}_{ref}.vcf.gz",
    log: "logs/normalise_small_variants/{caller}/{patient}_{ref}.log",
    conda: "../envs/bcftools.yaml",
    shell:
        """
        bcftools norm \
        -c w -m- \
        -f {input.ref} \
        {input.vcf} \
        -Oz -o {output} \
        &> {log}
        """

rule vep_annotate_small_variants_hg38:
    input:
        vcf="results/variants/{caller}/normalised/{patient}_hg38.vcf.gz",
        vep_data="resources/ref/hg38/ensembl-vep-data",
    output: "results/variants/{caller}/annotated/{patient}_hg38.vcf.gz",
    conda: "../envs/vep.yaml",
    log: "logs/vep_small_variants/{caller}/{patient}_hg38.log",
    params:
        options="--vcf --everything --compress_output bgzip --cache --fork 4 --force_overwrite"
    threads: 8,
    shell:
        """
        vep \
        {params.options} \
        --dir {input.vep_data} \
        -i {input.vcf} \
        -o {output} \
        &> {log}
        """

rule annotated_pass:
    input: "results/variants/{caller}/annotated/{patient}_{ref}.vcf.gz",
    output: "results/variants/{caller}/annotated/pass/{patient}_{ref}.vcf.gz",
    conda: "../envs/bcftools.yaml",
    log: "logs/annotated_pass/{caller}/{patient}_{ref}.log",
    shell:
        """
        bcftools filter \
        -i 'FILTER="PASS"' \
        -Oz -o {output} {input} \
        &> {log}
        """

# rule vep_annotate_dysgu:
#     input:
#         vcf="results/variants/dysgu/{sample}.vcf.gz",
#         vep_data="resources/ensembl-vep-data",
#     output: "results/variants/dysgu/annotated/{sample}.vcf.gz",
#     conda: "../envs/vep.yaml",
#     params:
#         options="--vcf --everything --compress_output bgzip --cache --fork 4 --force_overwrite"
#     threads: 8,
#     shell:
#         """
#         vep \
#         {params.options} \
#         --dir {input.vep_data} \
#         -i {input.vcf} \
#         -o {output} 
#         """        

# rule annotated_pass_dysgu:
#     input: "results/variants/dysgu/annotated/{sample}.vcf.gz",
#     output: "results/variants/dysgu/annotated/pass/{sample}.vcf.gz",
#     conda: "../envs/bcftools.yaml",
#     shell:
#         """
#         bcftools filter -i 'FILTER="PASS"' -Oz -o {output} {input}
#         """        
