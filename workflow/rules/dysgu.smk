rule dysgu_filter_pass:
    input: "results/variants/dysgu/{patient}_{sample}_{ref}.vcf"
    output: "results/variants/dysgu/{patient}_{sample}_{ref}_pass.vcf.gz"
    conda: "../envs/bcftools.yaml",
    shell:
        """
        bcftools filter -i 'FILTER="PASS"' -Oz -o {output} {input}
        """

rule dysgu_filter_fail:
    input: "results/variants/dysgu/{patient}_{sample}_{ref}.vcf"
    output: "results/variants/dysgu/{patient}_{sample}_{ref}_fail.vcf.gz"
    conda: "../envs/bcftools.yaml",
    shell:
        """
        bcftools filter -i 'FILTER!="PASS"' -Oz -o {output} {input}
        """

rule dysgu:
    input:
        ref="resources/ref/{ref}/{ref}.fa",
        cram=expand("results/alignment/{{patient}}_{{sample}}_{{ref}}.{ext}", ext=["cram", "cram.crai"]),
    output: temp("results/variants/dysgu/{patient}_{sample}_{ref}.vcf"),
    params:
        temp_dir = "dysgu_temp/{patient}_{sample}_{ref}/",
        model = "--diploid False",
    log: "logs/dysgu/{patient}_{sample}_{ref}.log"
    container: "docker://kcleal/dysgu",
    threads: 4,
    shell:
        """
        mkdir -p {params.temp_dir} &&
        dysgu run -x \
        --clean \
        -p {threads} \
        {params.model} \
        {input.ref} \
        {params.temp_dir} \
        {input.cram[0]} \
        > {output} \
        2> {log}
        """

