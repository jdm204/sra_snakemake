rule call_copy_segments:
    input:
        cr="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.cr.seg",
    output: "results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.cr.called.seg",
    conda: "../envs/gatk.yaml",
    log: "logs/call_copy_segments/{patient}_{tsample}_{ref}.log",
    shell:
        """
	gatk CallCopyRatioSegments \
        --input {input.cr} \
        --output {output} \
        &> {log}
        """

rule plot_modelled_segments:
    input:
        dict="resources/ref/{ref}/{ref}.dict",
        allelic_counts="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.hets.tsv",
        denoised_copyratios="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_denoised_copyratio.tsv",
        final_model_segments="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.modelFinal.seg",
    output: directory("results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segment_plots/"),
    conda: "../envs/gatk.yaml",
    params: "--minimum-contig-length 46709983",
    log: "logs/plot_modelled_segments/{patient}_{tsample}_{ref}.log",
    shell:
        """
	gatk PlotModeledSegments \
        --denoised-copy-ratios {input.denoised_copyratios} \
        --allelic-counts {input.allelic_counts} \
        --segments {input.final_model_segments} \
        --sequence-dictionary {input.dict} \
        {params} \
        --output {output} \
        --output-prefix {wildcards.patient}_{wildcards.tsample} \
        &> {log}
        """

rule model_segments:
    input:
        allelic_counts="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_allelic_counts.tsv",
        normal_allelic_counts=lambda wc: expand("results/variants/gatk_cnv/{{patient}}_{tsample}_{{ref}}_allelic_counts.tsv", tsample=patient_normal_samples(wc)),
        denoised_copyratios="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_denoised_copyratio.tsv",
    output:
        cr_seg="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.cr.seg",
        final_seg="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.modelFinal.seg",
        hets="results/variants/gatk_cnv/{patient}_{tsample}_{ref}_segments/{patient}_{tsample}.hets.tsv",
    conda: "../envs/gatk.yaml",
    params:
        normal_string=lambda wc, input: [f"--normal-allelic-counts {n} " for n in input.normal_allelic_counts],
        java_options="--java-options \"-Xmx4g\"",
        options="--maximum-number-of-smoothing-iterations 100",
    log: "logs/model_segments/{patient}_{tsample}_{ref}.log",
    shell:
        """
        gatk {params.java_options} \
        ModelSegments \
        {params.options} \
        --denoised-copy-ratios {input.denoised_copyratios} \
        --allelic-counts {input.allelic_counts} \
        {params.normal_string} \
        --output $(dirname {output.cr_seg}) \
        --output-prefix {wildcards.patient}_{wildcards.tsample} \
        &> {log}
        """

rule collect_allelic_counts:
    input:
        ref="resources/ref/{ref}/{ref}.fa",
        refidx="resources/ref/{ref}/{ref}.fa.fai",
        sites="resources/ref/{ref}/snps-only-af-only-gnomad_{ref}.vcf.gz",
        sitesidx="resources/ref/{ref}/snps-only-af-only-gnomad_{ref}.vcf.gz.tbi",
        cram="results/alignment/bqsr/{patient}_{sample}_{ref}.cram",
        crai="results/alignment/bqsr/{patient}_{sample}_{ref}.cram.crai",
    output: "results/variants/gatk_cnv/{patient}_{sample}_{ref}_allelic_counts.tsv",
    conda: "../envs/gatk.yaml",
    log: "logs/collect_allelic_counts/{patient}_{sample}_{ref}.log",
    threads: 4,
    shell:
        """
        gatk \
        CollectAllelicCounts \
        -L {input.sites} \
        -I {input.cram} \
        -R {input.ref} \
        -O {output} \
        &> {log}
        """

rule snps_only_gnomad:
    input: "resources/ref/{ref}/af-only-gnomad_{ref}.vcf.gz",
    output: "resources/ref/{ref}/snps-only-af-only-gnomad_{ref}.vcf.gz",
    conda: "../envs/bcftools.yaml",
    log: "logs/snps_only_gnomad/{ref}.log",
    params:
        regions=lambda wc: ",".join(CHROMOSOMES),
    shell:
        """
	bcftools filter \
        -r {params.regions} \
        -i 'TYPE="snp" & AF[0] > 0.05' \
        -Oz -o {output} {input} \
        &> {log}
        """

rule plot_denoised_read_counts:
    input:
        seqdict="resources/ref/{ref}/{ref}.dict",
        standardised="results/variants/gatk_cnv/{patient}_{sample}_{ref}_standardised_copyratio.tsv",
        denoised="results/variants/gatk_cnv/{patient}_{sample}_{ref}_denoised_copyratio.tsv",
    output: directory("results/variants/gatk_cnv/{patient}_{sample}_{ref}_copyratio_plots/"),
    conda: "../envs/gatk.yaml",
    params: "--minimum-contig-length 46709983", # len of smallest non-ALT contig in hg38
    log: "logs/plot_denoised_read_counts/{patient}_{sample}_{ref}.log",
    shell:
        """
        gatk PlotDenoisedCopyRatios \
        --standardized-copy-ratios {input.standardised} \
        --denoised-copy-ratios {input.denoised} \
        --sequence-dictionary {input.seqdict} \
        {params} \
        --output {output} \
        --output-prefix {wildcards.sample} \
        &> {log}
        """

rule denoise_read_counts:
    input:
        readcounts="results/variants/gatk_cnv/{patient}_{sample}_{ref}_readcounts.hdf5",
        readcount_pon="results/variants/gatk_cnv/readcount_pon_{ref}.hdf5",
        annotated_regions="resources/ref/{ref}/regions_annotated.tsv",
    output:
        standardised="results/variants/gatk_cnv/{patient}_{sample}_{ref}_standardised_copyratio.tsv",
        denoised="results/variants/gatk_cnv/{patient}_{sample}_{ref}_denoised_copyratio.tsv",
    conda: "../envs/gatk.yaml",
    params: java="--java-options \"-Xmx12g\"",
    log: "logs/denoise_read_counts/{patient}_{sample}_{ref}.log",
    shell:
        """
        gatk {params.java} DenoiseReadCounts \
        -I {input.readcounts} \
        --count-panel-of-normals {input.readcount_pon} \
        --annotated-intervals {input.annotated_regions} \
        --standardized-copy-ratios {output.standardised} \
        --denoised-copy-ratios {output.denoised} \
        &> {log}
        """

rule make_readcount_pon:
    input: expand("results/variants/gatk_cnv/{patient_normal_sample}_{{ref}}_readcounts.hdf5", patient_normal_sample=PATIENT_NORMAL_SAMPLES),
    output: "results/variants/gatk_cnv/readcount_pon_{ref}.hdf5",
    conda: "../envs/gatk.yaml",
    params:
        opts="--minimum-interval-median-percentile 10.0",
        java_opts='--java-options "-Xmx6500m"',
        input_str=lambda wc, input: ["-I " + infile for infile in input],
    log: "logs/make_readcount_PON/{ref}.log",
    shell:
        """
        gatk {params.java_opts} \
        CreateReadCountPanelOfNormals \
        {params.input_str} {params.opts} \
        -O {output} \
        &> {log}
        """
        
rule collect_read_counts:
    input:
        preproc_intervals="resources/ref/{ref}/regions_preprocessed.interval_list",
        ref="resources/ref/{ref}/{ref}.fa",
        cram="results/alignment/bqsr/{patient}_{sample}_{ref}.cram",
        crai="results/alignment/bqsr/{patient}_{sample}_{ref}.cram.crai",
    output: "results/variants/gatk_cnv/{patient}_{sample}_{ref}_readcounts.hdf5",
    conda: "../envs/gatk.yaml",            
    params: "--interval-merging-rule OVERLAPPING_ONLY"
    log: "logs/collect_read_counts/{patient}_{sample}_{ref}.log",
    shell:
        """
        gatk CollectReadCounts \
        -R {input.ref} \
        -I {input.cram} \
        -L {input.preproc_intervals} \
        {params} \
        -O {output} \
        &> {log}
        """

rule preprocess_intervals_WGS:
    input:
        ref="resources/ref/{ref}/{ref}.fa",
    output: "resources/ref/{ref}/regions_preprocessed.interval_list",
    conda: "../envs/gatk.yaml",
    params:
        "--bin-length 5000 --interval-merging-rule OVERLAPPING_ONLY",
    log: "logs/preprocess_intervals/{ref}.log",
    shell:
        """
        gatk PreprocessIntervals \
        -R {input.ref} \
        {params} \
        -O {output} \
        &> {log}
        """

rule annotate_intervals_gc:
    input:
        ref="resources/ref/{ref}/{ref}.fa",
        intervals="resources/ref/{ref}/regions.interval_list",
    output: "resources/ref/{ref}/regions_annotated.tsv",
    conda: "../envs/gatk.yaml",
    params: "--interval-merging-rule OVERLAPPING_ONLY",
    log: "logs/annotate_intervals_gc/{ref}.log",
    shell:
        """
        gatk AnnotateIntervals \
        -R {input.ref} \
        -L {input.intervals} \
        {params} \
        -O {output} \
        &> {log}
        """
        
rule bed_to_interval_list:
    input:
        bed=config["genomic_regions"],
        dict="resources/ref/{ref}/{ref}.dict",
    output: "resources/ref/{ref}/regions.interval_list",
    conda: "../envs/gatk.yaml",
    log: "logs/bed_to_interval_list/{ref}.log",
    shell:
        """
        gatk BedToIntervalList \
        -I {input.bed} -SD {input.dict} -O {output} \
        &> {log}
        """

