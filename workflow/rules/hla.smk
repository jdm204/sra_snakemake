rule hlatyping_nfcore:
    input:
        input="config/hla_units.csv",
    output: directory("results/hla_typing/"),
    params:
        pipeline="nf-core/hlatyping",
        revision="2.0.0",
        profile=["conda"],
        outdir="results/hla_typing",
        genome="GRCh38",
    handover: True
    wrapper: "v1.31.1/utils/nextflow"

rule make_hlatyping_samplesheet:
    output: "config/hla_units.csv",
    run:
        SAMPLE_TABLE.with_columns([
            pl.format("{}_{}_{}", "patient", "sample", "unit").alias("sample")
        ])\
                    .filter(pl.col("datatype") == "DNA")\
                    .drop(["patient", "sampletype", "unit"])\
                    .pivot(values="path", index=["sample", "datatype"], columns="pair", aggregate_function=None)\
                    .rename({"1": "fastq_1", "2": "fastq_2", "datatype":"seq_type"})\
                    .select(["sample", "fastq_1", "fastq_2",\
                             pl.col("seq_type").apply(lambda x: x.lower())])\
                    .write_csv(f"{output}")
        
