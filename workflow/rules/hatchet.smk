rule compute_cn:
    input:
        seg="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}.seg",
        bbc="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}.bbc",
    output: directory("results/hatchet/compute-cn/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}/"),
    conda: "../envs/hatchet.yaml",
    threads: 16,
    params:
        prefix="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}",
        clones="2,6",
        options="-l 0.5 -u 0.06 -p 100 "
    shell:
        """
        mkdir -p {output} && \
        hatchet compute-cn -i {params.prefix} -x {output} --jobs {threads} \
        -n {params.clones} {params.options}
        """

rule plot_bins:
    input:
        seg="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}.seg",
        bins="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}.bbc",
    output: directory("results/hatchet/plot-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}"),
    conda: "../envs/hatchet.yaml",
    threads: 5,
    shell:
        """
        mkdir -p {output} && \
        hatchet plot-bins -c CBB --ymin 0 --ymax 8 -tS 0.005 \
        -x {output} -s {input.seg} {input.bins}
        """

rule cluster_bins:
    input: "results/hatchet/combine-counts/{patient}_{binsize}.bb",
    output:
        segments="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}.seg",
        bins="results/hatchet/cluster-bins/{patient}_{binsize}_dbaf{dipbaf}_trdr{trdr}_tbaf{tbaf}_maxc{maxc}.bbc",
    conda: "../envs/hatchet.yaml",
    threads: 5,
    params:
        diploidbaf=0.1,
        tolerancerdr=0.1,
        tolerancebaf=0.1,
        bootclustering=20,
        ratiodeviation=0.002,
        bafdeviation=0.002,
        seed=2021,
        concentration=0.02,
        restarts=10,
    shell:
        """
        hatchet cluster-bins -o {output.segments} -O {output.bins} \
        -d {wildcards.dipbaf} -tR {wildcards.trdr} -tB {wildcards.tbaf} \
        -u {params.bootclustering} -dR {params.ratiodeviation} -dB {params.bafdeviation} \
        -e {params.seed} -K {wildcards.maxc} -c {params.concentration} -R {params.restarts} \
        {input}
        """

rule combine_counts:
    input:
        normalbins="results/hatchet/count-reads/{patient}_{binsize}_normal.tsv",
        tumourbins="results/hatchet/count-reads/{patient}_{binsize}_tumour.tsv",
        tumourbafs="results/hatchet/count-alleles/{patient}_tumour.tsv",
    output: "results/hatchet/combine-counts/{patient}_{binsize}.bb",
    conda: "../envs/hatchet.yaml",
    threads: 22,
    params:
        #blocklength="50kb",
        diploidbaf="0.1",
    shell:
        """
        hatchet combine-counts \
        -c {input.normalbins} -C {input.tumourbins} -B {input.tumourbafs} \
        -d {params.diploidbaf} > {output}
        """
#-l {params.blocklength}

# WIP
rule whatshap_phase_vcf_hatchet:
    input:
        ref=config["ref"]["fa"],
        bam=lambda wc: expand("results/mapped/bqsr/{normal}.bam",
                                 normal = patient_normal_sample(wc)),
        vcf="{path_to}/{filename}.vcf.gz",
    output: "{path_to}/{filename}_phased.vcf.gz",
    conda: "../envs/whatshap.yaml",
    params: "--indels",
    shell:
        """
        whatshap phase \
        {params} \
        -o {output} \
        --reference {input.ref} \
        {input.vcf} {input.bams}
        """

rule count_alleles:
    input:
        snps="results/hatchet/genotype-snps/{patient}",
        ref=config["ref"]["fa"],
        normal=lambda wc: expand("results/mapped/bqsr/{normal}.bam",
                                 normal = patient_normal_sample(wc)),
        normal_idx=lambda wc: expand("results/mapped/bqsr/{normal}.bam.bai",
                                 normal = patient_normal_sample(wc)),
        tumours=lambda wc: expand("results/mapped/bqsr/{tumour}.bam",
                                 tumour = patient_tumour_samples(wc)),
        tumours_idx=lambda wc: expand("results/mapped/bqsr/{tumour}.bam.bai",
                                 tumour = patient_tumour_samples(wc)),
    output:
        normal="results/hatchet/count-alleles/{patient}_normal.tsv",
        tumour="results/hatchet/count-alleles/{patient}_tumour.tsv",
        snps=directory("results/hatchet/count-alleles/{patient}_snps"),
    conda: "../envs/hatchet.yaml",
    threads: 22,
    params:
        samples=lambda wc: " ".join(patient_normal_sample(wc)+patient_tumour_samples(wc)),
        mincov=20,
        maxcov=600,
        minmapq=10,
        regions=lambda wc: config["regions"],
    shell:
        """
        mkdir -p {output.snps} && touch {output.snps}/Normal_chr{{1..22}}_bcftools.log &&
        hatchet count-alleles -j {threads} --reference {input.ref} \
        -N {input.normal[0]} --snps {input.snps}/*.vcf.gz \
        -T {input.tumours} \
        -S {params.samples} \
        --mincov {params.mincov} --maxcov {params.maxcov} --readquality {params.minmapq} \
        -O {output.normal} -o {output.tumour} -l {output.snps}
        """
#--regions {params.regions} \

rule genotype_snps:
    input:
        ref=config["ref"]["fa"],
        normal=lambda wc: expand("results/mapped/bqsr/{normal}.bam",
                                 normal = patient_normal_sample(wc)),
    output: directory("results/hatchet/genotype-snps/{patient}"),
    conda: "../envs/hatchet.yaml",
    threads: 22,
    shell:
        """
        mkdir -p {output} &&
        hatchet genotype-snps \
        -j {threads} \
        --reference {input.ref} \
        -N {input.normal} \
        -o {output}
        """

rule count_reads:
    input:
        ref=config["ref"]["fa"],
        normal=lambda wc: expand("results/mapped/bqsr/{normal}.bam",
                                 normal = patient_normal_sample(wc)),
        tumours=lambda wc: expand("results/mapped/bqsr/{tumour}.bam",
                                 tumour = patient_tumour_samples(wc)),
    output:
        normal="results/hatchet/count-reads/{patient}_{binsize}_normal.tsv",
        tumour="results/hatchet/count-reads/{patient}_{binsize}_tumour.tsv",
        total="results/hatchet/count-reads/{patient}_{binsize}_total.tsv",
    params:
        binsize=lambda wc: wc.binsize, # default was "50kb",
        regions=config["regions"],
    conda: "../envs/hatchet.yaml",
    threads: 22,
    shell:
        """
        hatchet count-reads \
        -j {threads} \
        --reference {input.ref} \
        -b {params.binsize} \
        -N {input.normal} \
        -T {input.tumours} \
        -O {output.normal} \
        -o {output.tumour} \
        -t {output.total}
        """
