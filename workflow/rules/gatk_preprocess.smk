rule gatk_bqsr_2:
    input:
        cram="results/alignment/deduplication/{patient}_{sample}_{ref}.cram",
        ref="resources/ref/{ref}/{ref}.fa.gz",
        refidx="resources/ref/{ref}/{ref}.fa.gz.fai",
        refdict="resources/ref/{ref}/{ref}.dict",
        recal_table="results/qc/base_recalibration/{patient}_{sample}_{ref}.txt",
    output: "results/alignment/bqsr/{patient}_{sample}_{ref}.cram",
    conda: "../envs/gatk.yaml",
    log: "logs/apply_BQSR/{patient}_{sample}_{ref}.log",
    shell:
        """
        gatk ApplyBQSR \
        -R {input.ref} -I {input.cram} -O {output} \
        --bqsr-recal-file {input.recal_table} \
        &> {log}
        """
        
rule gatk_bqsr_1:
    input:
        cram="results/alignment/deduplication/{patient}_{sample}_{ref}.cram",
        ref="resources/ref/{ref}/{ref}.fa.gz",
        refidx="resources/ref/{ref}/{ref}.fa.gz.fai",
        refdict="resources/ref/{ref}/{ref}.dict",
        known_variation="resources/ref/{ref}/dbsnp138_{ref}.vcf.gz",
        known_variation_idx="resources/ref/{ref}/dbsnp138_{ref}.vcf.gz.tbi",
    output: "results/qc/base_recalibration/{patient}_{sample}_{ref}.txt",
    conda: "../envs/gatk.yaml",
    log: "logs/base_recalibrator/{patient}_{sample}_{ref}.log",
    shell:
        """
        gatk BaseRecalibrator \
        -I {input.cram} -R {input.ref} -O {output} \
        --known-sites {input.known_variation} \
        &> {log}
        """
        
rule dedup:
    input:
        cram="results/alignment/{patient}_{sample}_{ref}.cram",
        ref="resources/ref/{ref}/{ref}.fa.gz",
    output:
        metrics="results/qc/deduplication/{patient}_{sample}_{ref}.txt",
        cram="results/alignment/deduplication/{patient}_{sample}_{ref}.cram",
    conda: "../envs/gatk.yaml",
    log: "logs/deduplication/{patient}_{sample}_{ref}.log",
    shell:
        """
        gatk MarkDuplicates \
        -R {input.ref} \
        -I {input.cram} \
        -M {output.metrics} -O {output.cram} \
        &> {log}
        """
