rule cram_pileup:
    input:
        ref="resources/ref/{ref}/{ref}.fa.gz",
        cram="results/alignment/{patient}_{sample}_{ref}.cram",
    output: "results/pileup/{patient}_{sample}_{ref}.pileup",
    conda: "../samtools.yaml",
    log: "logs/cram_pileup/{patient}_{sample}_{ref}.log"
    shell:
        """
        (
        samtools mpileup \
        {input.ref} \
        -o {output} \
        {input.cram}
        ) &> {log}
        """
