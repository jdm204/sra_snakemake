rule mutect2:
    input:
        ref="resources/ref/{ref}/{ref}.fa.gz",
        refidx="resources/ref/{ref}/{ref}.fa.gz.gzi",
        refdict="resources/ref/{ref}/{ref}.dict",
        crams=patient_sample_crams,
        crai=lambda wc: [f"{cram}.crai" for cram in patient_sample_crams(wc)],
        pop_afs="resources/ref/{ref}/af-only-gnomad_{ref}.vcf.gz",
        pon="resources/ref/{ref}/1000g_pon_{ref}.vcf.gz",
    output:
        vcf="results/variants/mutect2/raw/{patient}_{chr}_{ref}.vcf.gz",
        f1r2="results/variants/mutect2/raw/{patient}_{chr}_{ref}_f1r2.tar.gz",
        stats="results/variants/mutect2/raw/{patient}_{chr}_{ref}.vcf.gz.stats",
    log: "logs/mutect2/{patient}_{chr}_{ref}.log",
    conda: "../envs/gatk.yaml",
    threads: 8,
    params:
        input_string=lambda wc, input: [f"-I {cram} " for cram in input.crams],
        normal_string=lambda wc: [f"-normal {id} " for id in patient_normal_samples(wc)],
    shell:
        """
        gatk Mutect2 -R {input.ref} -L {wildcards.chr} -pon {input.pon} \
        {params.input_string} {params.normal_string} -O {output.vcf} \
        --germline-resource {input.pop_afs} --f1r2-tar-gz {output.f1r2} \
        > {log} 2>&1
        """

rule merge_mutect2_vcfs:
    input: expand("results/variants/mutect2/raw/{{patient}}_{chr}_{{ref}}.vcf.gz", chr=CHROMOSOMES),
    output: "results/variants/mutect2/raw/{patient}_{ref}.vcf.gz",
    conda: "../envs/gatk.yaml",
    log: "logs/merge_mutect2_vcfs/{patient}_{ref}.log",
    params:
        input_string=lambda wildcards, input: [f"-I {i}" for i in input],
    shell:
        """
        gatk MergeVcfs \
        {params.input_string} -O {output} \
        &> {log}
        """

rule merge_mutect2_stats:
    input: expand("results/variants/mutect2/raw/{{patient}}_{chr}_{{ref}}.vcf.gz.stats", chr=CHROMOSOMES),
    output: "results/variants/mutect2/raw/{patient}_{ref}.vcf.gz.stats",
    conda: "../envs/gatk.yaml",
    params: stats=lambda wc, input: [f"--stats {file} " for file in input],
    log: "logs/merge_mutect2_stats/{patient}_{ref}.log",            
    shell:
        """
        gatk MergeMutectStats \
        {params.stats} -O {output} \
        &> {log}
        """

rule learn_read_orientation:
    input: expand("results/variants/mutect2/raw/{{patient}}_{chr}_{{ref}}_f1r2.tar.gz", chr=CHROMOSOMES),
    output: "results/variants/mutect2/raw/{patient}_{ref}_read_orientation_model.tar.gz",
    conda: "../envs/gatk.yaml",
    params: input_string=lambda wc, input: [f"-I {f1r2}" for f1r2 in input],
    log: "logs/learn_read_orientation/{patient}_{ref}.log",
    shell:
        """
        gatk LearnReadOrientationModel \
        {params.input_string} -O {output} \
        &> {log}
        """

rule pileup_summaries:
    input:
        crams=patient_sample_crams,
        crai=lambda wc: [f"{cram}.crai" for cram in patient_sample_crams(wc)],
        ref="resources/ref/{ref}/{ref}.fa.gz",
        common_variants="resources/ref/{ref}/small_exac_common_3_{ref}.vcf.gz",
    output: "results/variants/mutect2/raw/{patient}_{ref}_pileupsummaries.table",
    conda: "../envs/gatk.yaml",
    params: input_string=lambda wc, input: [f"-I {path} " for path in input.crams],
    log: "logs/pileup_summaries/{patient}_{ref}.log",
    shell:
        """
        gatk GetPileupSummaries \
        {params.input_string} -V {input.common_variants} \
        -L {input.common_variants} \
        -R {input.ref} \
        -O {output} \
        &> {log}
        """        

rule calculate_contamination:
    input: "results/variants/mutect2/raw/{patient}_{ref}_pileupsummaries.table",
    output: "results/variants/mutect2/raw/{patient}_{ref}_contamination.table",
    conda: "../envs/gatk.yaml",
    log: "logs/calculate_contamination/{patient}_{ref}.log",
    shell:
        """
        gatk CalculateContamination \
        -I {input} -O {output} \
        &> {log}
        """

rule filter_mutect2:
    input:
        ref="resources/ref/{ref}/{ref}.fa.gz",
        vcf="results/variants/mutect2/raw/{patient}_{ref}.vcf.gz",
        rom="results/variants/mutect2/raw/{patient}_{ref}_read_orientation_model.tar.gz",
        stats="results/variants/mutect2/raw/{patient}_{ref}.vcf.gz.stats",
        contamination="results/variants/mutect2/raw/{patient}_{ref}_contamination.table",
    output: "results/variants/mutect2/filtered/{patient}_{ref}.vcf.gz",
    conda: "../envs/gatk.yaml",
    log: "logs/filter_mutect2/{patient}_{ref}.log",
    shell:
        """
        gatk FilterMutectCalls \
        -V {input.vcf} \
        --ob-priors {input.rom} \
        --stats {input.stats} \
        --contamination-table {input.contamination} \
        -R {input.ref} \
        -O {output} \
        &> {log}
        """
