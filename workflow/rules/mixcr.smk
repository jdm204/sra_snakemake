if config["enable"]["immune_receptor"] and not os.path.isfile(config["mixcr_license_file"]):
        eprint("you need an academic (free) or commercial license to run MiXCR; please include a path to it in the config file")
        sys.exit()

# rule immunarch:
#     input: "results/mixcr/",
#     output: "results/mixcr/immunarch.png",
#     conda: "../envs/immunarch.yaml",
#     script: "../scripts/immunarch.R"

# rule make_immunarch_metadata:
#     output: "results/mixcr/metadata.txt",
#     params: SAMPLE_TABLE,
#     run:
#         with open(output[0], "w") as out:
#             out.write("Sample\tPatient\tSampleType\tSex\tAge\n")
#             for index, row in params[0].iterrows():
#                 sample = row["Sample Name"] + ".txt.clonotypes.ALL"
#                 patient = row.patient
#                 sampletype = row.sampletype
#                 sex = row.sex
#                 age = row.AGE
#                 out.write(f"{sample}\t{patient}\t{sampletype}\t{sex}\t{age}\n")

rule cram_to_fastqs:
    input:
        ref="resources/ref/{ref}/{ref}.fa.gz",
        cram="results/alignment/{patient}_{sample}_{ref}.cram",
    output:
        R1=temp("results/alignment/{patient}_{sample}_{ref}_1.fastq.gz"),
        R2=temp("results/alignment/{patient}_{sample}_{ref}_2.fastq.gz"),
    log: "logs/cram_to_fastqs/{patient}_{sample}_{ref}.log",
    conda: "../envs/samtools.yaml",
    shell:
        """
        (
        samtools fastq \
        --reference {input.ref} \
        -1 {output.R1} \
        -2 {output.R2} \
        -0 /dev/null \
        -s /dev/null \
        {input.cram} \
        ) &> {log}
        """

rule mixcr:
    input:
        R1="results/alignment/{patient}_{sample}_{ref}_1.fastq.gz",
        R2="results/alignment/{patient}_{sample}_{ref}_2.fastq.gz",
    output: "results/mixcr/{patient}_{sample}_{ref}.vdjca",
    params:
        output_prefix="results/mixcr/{patient}_{sample}_{ref}",
        license_file=config["mixcr_license_file"],
    log: "logs/mixcr/{patient}_{sample}_{ref}.log",
    conda: "../envs/mixcr.yaml",
    threads: 20,
    shell:
        """
        (
        MI_LICENSE_FILE={params.license_file} \
        mixcr analyze exome-full-length \
        --species hsa \
        --threads {threads} \
        {input.R1} {input.R2} \
        {params.output_prefix} \
        ) &> {log} 
        """
