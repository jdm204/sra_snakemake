if config["aligner"] == "bwa-mem2":
    ruleorder: mapping_bwamem2 > mapping_bwamem
else:
    ruleorder: mapping_bwamem > mapping_bwamem2

rule merge_unit_alignments:
    input:
        crams=sample_unit_crams,
        ref="resources/ref/{ref}/{ref}.fa.gz",
    output: "results/alignment/{patient}_{sample}_{ref}.cram",
    log: "logs/samtools_merge/{patient}_{sample}_{ref}.log",
    conda: "../envs/samtools.yaml",
    shell:
        """
        if [ $(echo {input.crams} | wc -w) -eq 1 ]
        then
        mv {input.crams} {output}
        else
        (
        samtools merge \
        --reference {input.ref} -O CRAM \
        -o {output} {input.crams}
        ) &> {log}
        fi
        """

rule mapping_bwamem:
    input:
        reads=unit_human_fastqs,
        ref="resources/ref/{ref}/{ref}.fa.gz",
        refindex="resources/ref/{ref}/{ref}.fa.gz.bwt",
    output: temp("results/alignment/units/{patient}_{sample}_{unit}_{ref}.cram"),
    log: "logs/bwa_mem/{patient}_{sample}_{unit}_{ref}.log",
    params:
        readgroup=r"-R '@RG\tID:{sample}_{unit}\tSM:{sample}\tPL:ILLUMINA'",
    threads: 56,
    conda: "../envs/bwa.yaml",
    shell:
        """
        (
        bwa mem \
        {params.readgroup} \
        {input.ref} \
        {input.reads} \
        | samtools sort \
        --reference {input.ref} -O CRAM \
        -o {output} \
        ) &> {log}
        """

rule bwa_index:
    input: "resources/ref/{ref}/{ref}.fa.gz",
    output: multiext("resources/ref/{ref}/{ref}.fa.gz", ".amb", ".ann", ".bwt", ".pac", ".sa"),
    log: "logs/bwa_index/{ref}.log",
    conda: "../envs/bwa.yaml",
    shell:
        """
        bwa index -a bwtsw {input}
        """

rule mapping_bwamem2:
    input:
        reads=unit_human_fastqs,
        ref="resources/ref/{ref}/{ref}.fa.gz",
        idx="resources/ref/{ref}/{ref}.fa.gz.bwt.2bit.64",
    output: "results/alignment/units/{patient}_{sample}_{unit}_{ref}.cram",
    log: "logs/bwa_mem2/{patient}_{sample}_{unit}_{ref}.log",
    params:
        readgroup=r"-R '@RG\tID:{sample}_{unit}\tSM:{sample}\tPL:ILLUMINA'",
    threads: 32,
    conda: "../envs/bwa-mem2.yaml",
    shell:
        """
        (
        bwa-mem2 mem \
        -t {threads} \
        {params.readgroup} \
        {input.ref} \
        {input.reads} \
        | samtools sort \
        --reference {input.ref} \
        -O CRAM -o {output} \
        ) &> {log}
        """

rule bwa_mem2_index:
    input: "{genome}",
    output:
        "{genome}.0123",
        "{genome}.amb",
        "{genome}.ann",
        "{genome}.bwt.2bit.64",
        "{genome}.pac",
    log: "logs/bwa-mem2_index/{genome}.log",
    params:
        prefix=lambda wc: wc.genome,
    wrapper: "v1.27.0/bio/bwa-mem2/index"

rule index_bam:
    input: "{arbitrary_path}/{filename}.bam",
    output: "{arbitrary_path}/{filename}.bam.bai",
    conda: "../envs/samtools.yaml",
    shell: "samtools index {input}"

rule index_cram:
    input: "{arbitrary_path}/{filename}.cram",
    output: "{arbitrary_path}/{filename}.cram.crai",
    conda: "../envs/samtools.yaml",
    shell: "samtools index {input}"
