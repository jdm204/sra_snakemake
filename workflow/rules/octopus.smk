# rule tabix_index:
#     input: "{path}/{filename}.vcf.gz",
#     output: "{path}/{filename}.vcf.gz.tbi",
#     conda: "../envs/samtools.yaml",
#     shell: "tabix {input}"

rule octopus_concat_chromosome:
    input: expand("results/variants/octopus/filtered/{{patient}}_{chr}_{{ref}}.vcf.gz", chr=CHROMOSOMES),
    output: "results/variants/octopus/filtered/{patient}_{ref}.vcf.gz",
    conda: "../envs/bcftools.yaml",
    shell: "bcftools concat {input} -Oz -o {output} "

def octopus_normal_string(wc):
    return [f"-N {sample} " for sample in patient_normal_samples(wc)]

rule octopus_chromosome:
    input:
        reads=patient_sample_crams,
        crai=lambda wc: map(lambda x: x + ".crai", patient_sample_crams(wc)),
        reffa="resources/ref/{ref}/{ref}.fa",
        reffai="resources/ref/{ref}/{ref}.fa.fai",
        germlineforest="resources/germline.forest",
        somaticforest="resources/somatic.forest",
    output:
        vcf=temp("results/variants/octopus/filtered/{patient}_{chr}_{ref}.vcf.gz"),
        tbi=temp("results/variants/octopus/filtered/{patient}_{chr}_{ref}.vcf.gz.tbi"),
    threads: 56,
    #container: "docker://dancooke/octopus:0.7.4",
    conda: "../envs/octopus.yaml",
    params:
        normal_string=octopus_normal_string,
        annotations="AD ADP AF AFB",
        tuning="",
    shell:
        "octopus --threads {threads} -R {input.reffa} "
        "--forest {input.germlineforest} --somatic-forest {input.somaticforest} "
        "--annotations {params.annotations} {params.tuning} --regions {wildcards.chr} "
        "-I {input.reads} {params.normal_string} -o {output.vcf} "

rule get_forests:
    output:
        germline="resources/germline.forest.gz",
        somatic="resources/somatic.forest.gz",
    conda: "../envs/gsutil.yaml",
    params:
        germline="gs://luntergroup/octopus/forests/germline.v0.7.4.forest.gz",
        somatic="gs://luntergroup/octopus/forests/somatic.v0.7.4.forest.gz",
    shell: "gsutil cp {params.germline} {output.germline} && "
           "gsutil cp {params.somatic} {output.somatic} "

rule decompress_forest:
    input: "resources/{forest}.forest.gz",
    output: temp("resources/{forest}.forest"),
    shell: "gzip -dc {input} > {output} "
