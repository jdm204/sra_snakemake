REFERENCE_FASTA_URL = dict({
    "hg38": "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz",
    "hg38-transcriptome": "https://ftp.ensembl.org/pub/release-111/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz",
    "noncodev6": "http://www.noncode.org/datadownload/NONCODEv6_human.fa.gz",
    "chm13": "https://s3-us-west-2.amazonaws.com/human-pangenomics/T2T/CHM13/assemblies/analysis_set/chm13v2.0_maskedY.fa.gz"})

REFERENCE_DBSNP_URL = dict({
    "hg38": "gs://gcp-public-data--broad-references/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf"})

REFERENCE_GNOMAD_URL = dict({
    "hg38": "gs://gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz"})

REFERENCE_1000GPON_URL = dict({
    "hg38": "gs://gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz"})

REFERENCE_EXACCOMMON_URL = dict({
    "hg38": "gs://gatk-best-practices/somatic-hg38/small_exac_common_3.hg38.vcf.gz"})

REFERENCE_GTF_URL = dict({
    "hg38-noncodev6" : "http://www.noncode.org/datadownload/NONCODEv6_human_hg38_lncRNA.gtf.gz"})

rule get_reference_genome:
    output: "resources/ref/{ref}/{ref}.fa.gz",
    params:
        url=lambda wc: REFERENCE_FASTA_URL[wc.ref],
        ua='--user-agent "Mozilla/4.0"',
    conda: "../envs/curl.yaml"
    shell:
        "curl {params.ua} -L {params.url} | bgzip -dc | bgzip -c > {output} "

rule get_salmon_index_hg38:
    output: directory("resources/ref/hg38/salmon_sa_index"),
    params:
        url="http://refgenomes.databio.org/v3/assets/archive/2230c535660fb4774114bfa966a62f823fdb6d21acf138d4/salmon_sa_index?tag=default",
        ua='--user-agent "Mozilla/4.0"',
    conda: "../envs/curl.yaml"
    shell:
        "mkdir {output} && curl {params.ua} -L {params.url} | tar xzf - -C {output} --strip-components=1"

rule tx2gene_noncodev6:
    output: "resources/ref/noncodev6/tx2gene.tsv.gz",
    params:
        url=REFERENCE_GTF_URL["hg38-noncodev6"],
        ua='--user-agent "Mozilla/4.0"',
    conda: "../envs/curl.yaml",
    shell:
        r"""
        curl {params.ua} -L {params.url} \
        | gzip -dc \
        | sd '^.*gene_id "([A-Z0-9\.]+).*"; transcript_id "([A-Z0-9\.]+)".*;.*' '$2\t$1' \
        | uniq | gzip > {output}
        """

rule tx2gene_hg38:
    input: "resources/ref/hg38-transcriptome/hg38-transcriptome.fa.gz",
    output: "resources/ref/hg38-transcriptome/tx2gene.tsv.gz",
    conda: "../envs/curl.yaml",
    shell:
        r"""
        zcat {input} \
        | grep '>' \
        | sd '>([A-Z0-9]*\.[0-9]).*gene:([A-Z0-9]*\.[0-9]).*' '$1\t$2' \
        | uniq | gzip > {output}
        """

rule tx2gene_hg38_noncodev6:
    input:
        hg38="resources/ref/hg38-transcriptome/tx2gene.tsv.gz",
        noncodev6="resources/ref/noncodev6/tx2gene.tsv.gz",
    output: "resources/ref/hg38-noncodev6-genscriptome/tx2gene.tsv.gz",
    conda: "../envs/curl.yaml",
    shell:
        """
        zcat {input.hg38} {input.noncodev6} \
        | gzip > {output}
        """

rule get_known_variation:
    output: "resources/ref/{ref}/dbsnp138_{ref}.vcf.gz",
    params:
        url=lambda wc: REFERENCE_DBSNP_URL[wc.ref],
        uncompressed="resources/ref/{ref}/dbsnp138_{ref}.vcf",
    conda: "../envs/gsutil.yaml",
    shell:
        """
        gsutil cp {params.url} {params.uncompressed} \
        && gzip {params.uncompressed}
        """

rule get_100g_panel_of_normals:
    output:
        vcf="resources/ref/{ref}/1000g_pon_{ref}.vcf.gz",
        idx="resources/ref/{ref}/1000g_pon_{ref}.vcf.gz.tbi",
    params:
        url=lambda wc: REFERENCE_1000GPON_URL[wc.ref],
    conda: "../envs/gsutil.yaml",
    shell:
        """
        gsutil cp {params.url} {output.vcf} \
        && gsutil cp {params.url}.tbi {output.idx}
        """

rule get_gnomad_afs:
    output:
        vcf="resources/ref/{ref}/af-only-gnomad_{ref}.vcf.gz",
        idx="resources/ref/{ref}/af-only-gnomad_{ref}.vcf.gz.tbi",
    params:
        url=lambda wc: REFERENCE_GNOMAD_URL[wc.ref],
    conda: "../envs/gsutil.yaml",
    shell:
        """
        gsutil cp {params.url} {output.vcf} \
        && gsutil cp {params.url}.tbi {output.idx}
        """

rule get_small_common_variants:
    output:
        vcf="resources/ref/{ref}/small_exac_common_3_{ref}.vcf.gz",
        idx="resources/ref/{ref}/small_exac_common_3_{ref}.vcf.gz.tbi"
    params:
        url=lambda wc: REFERENCE_EXACCOMMON_URL[wc.ref],
    conda: "../envs/gsutil.yaml",
    shell:
        """
        gsutil cp {params.url} {output.vcf} \
        && gsutil cp {params.url}.tbi {output.idx}
        """
        
rule tabix_index:
    input: "{path}/{filename}.vcf.gz",
    output: "{path}/{filename}.vcf.gz.tbi",
    conda: "../envs/samtools.yaml",
    shell: "tabix {input}"

rule fasta_dict:
    input: "{somepath}/{somefasta}.fa.gz",
    output: "{somepath}/{somefasta}.dict",
    conda: "../envs/gatk.yaml",
    shell: "gatk CreateSequenceDictionary -R {input} "

rule fasta_gz_index:
    input: "{somepath}/{somefasta}.fa.gz",
    output: multiext("{somepath}/{somefasta}.fa.gz", ".fai", ".gzi"),
    conda: "../envs/samtools.yaml",
    shell: "samtools faidx {input}"

rule decompress_fasta:
    input: "{somepath}/{somefasta}.fa.gz",
    output: "{somepath}/{somefasta}.fa",
    conda: "../envs/samtools.yaml",
    shell: "bgzip -dc {input} > {output}"      

rule fasta_index:
    input: "{somepath}/{somefasta}.fa",
    output: "{somepath}/{somefasta}.fa.fai",
    conda: "../envs/samtools.yaml",
    shell: "samtools faidx {input}"           
           
rule index_vcf_gz:
    input: "{path_to_dir}/{basename}.vcf.gz",
    output: "{path_to_dir}/{basename}.vcf.gz.tbi,"
    conda: "../envs/samtools.yaml",
    shell:
        """
        tabix -p vcf {input}
        """        
        
# rule gzip_vcf:
#     input: "{path_to_dir}/{basename}.vcf",
#     output: "{path_to_dir}/{basename}.vcf.gz",
#     conda: "../envs/samtools.yaml",
#     shell:
#         """
#         cat {input} | bgzip > {output}
#         """

rule get_vep_data:
    output: directory("resources/ref/hg38/ensembl-vep-data"),
    conda: "../envs/vep.yaml",
    shell:
        "vep_install -n -a cf -s homo_sapiens -y GRCh38 -c {output} --CONVERT"
