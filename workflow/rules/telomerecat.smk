rule telomerecat:
    input:
        cram="results/alignment/{patient}_{sample}_{ref}.cram",
        ref="resources/ref/{ref}/{ref}.fa.gz",
    output: "results/telomere/{patient}_{sample}_{ref}.csv",
    container: "docker://quay.io/wtsicgp/telomerecat:4.0.2",
    threads: 8,
    shell:
        """
        telomerecat bam2length \
        -p {threads} \
        --reference {input.ref} \
        --output {output} \
        {input.cram}
        """
