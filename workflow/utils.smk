def pprint(str):
    "print a string in bold, with color"
    print('\033[94m' + '\033[1m' + str + '\033[0m')

def eprint(str):
    "print a string to stderr in color"
    print('\033[91m' + '\033[1mError: ' + str + '\033[0m', file=sys.stderr)

def uniq(lst):
    "deduplicate a list"
    return list(set(lst))
