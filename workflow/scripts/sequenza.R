library(sequenza)
test <- sequenza.extract(snakemake@input[[1]], chromosome.list = paste0("chr", c(1:22,"X","Y")))

CP <- sequenza.fit(test)

sequenza.results(sequenza.extract = test,
                 cp.table = CP, sample.id = basename(snakemake@input[[1]]),
                 out.dir=snakemake@output[[1]])
