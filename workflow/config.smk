CONFIGFILE = "config/config.yaml"
EXAMPLE_CONFIGFILE = "config/example_config.yaml"

# if user hasn't provided a config file, use an example
if os.path.exists(CONFIGFILE):
    pprint(f"Using custom config file: {CONFIGFILE}")
    configfile: CONFIGFILE
elif os.path.exists(EXAMPLE_CONFIGFILE):
    pprint(f"Using example config file: {EXAMPLE_CONFIGFILE}")
    configfile: EXAMPLE_CONFIGFILE
else:
    eprint(f"missing {CONFIGFILE} and {EXAMPLE_CONFIGFILE}")
    sys.exit("Quitting without configuration")

# get sample information from a metadata table
pprint(f"Reading and validating sample table: {config['units']}")
SAMPLE_TABLE = pl.read_csv(config["units"], separator="\t")

validate(SAMPLE_TABLE.to_pandas(), "schemas/sample_table_schema.yaml")

wildcard_constraints:
    patient="[A-Za-z0-9-]+",
    sample="[A-Za-z0-9-]+",
    unit="[A-Za-z0-9-_]+",
    chr="chr[0-9A-Z]+",
    ref="[a-z0-9A-Z-]+",
    formula="BY_[^@]+",
    comparison="[A-Za-z0-9@-_]+",
