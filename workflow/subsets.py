PATIENTS = SAMPLE_TABLE.select("patient").to_series().unique().to_list()
PATIENTS_WITH_GERMLINE = SAMPLE_TABLE.filter(pl.col("sampletype").str.contains("normal"))\
                                     .select("patient").to_series()\
                                     .unique().to_list()
PATIENTS_WITH_RELAPSE = SAMPLE_TABLE.filter(pl.col("sampletype").str.contains("relapse"))\
                                     .select("patient").to_series()\
                                     .unique().to_list()
PATIENTS_WITH_PDX = SAMPLE_TABLE.filter(pl.col("sampletype").str.contains("PDX"))\
                                     .select("patient").to_series()\
                                     .unique().to_list()
PATIENTS_WITHOUT_GERMLINE = set(PATIENTS) - set(PATIENTS_WITH_GERMLINE)

PATIENT_SAMPLES = {f"{r['patient']}_{r['sample']}" for r in SAMPLE_TABLE.iter_rows(named=True)}
PATIENT_SAMPLE_UNITS = {f"{r['patient']}_{r['sample']}_{r['unit']}" for r in SAMPLE_TABLE.iter_rows(named=True)}
PATIENT_NORMAL_SAMPLES = {f"{r['patient']}_{r['sample']}" for r in SAMPLE_TABLE.filter(pl.col("sampletype").str.contains("normal")).iter_rows(named=True)}

PATIENT_TUMOUR_SAMPLES = {f"{r['patient']}_{r['sample']}" for r in SAMPLE_TABLE.filter(pl.col("sampletype").str.contains("cancer|tumour|relapse|metastasis|PDX")).iter_rows(named=True)}
PATIENT_RNA_SAMPLES = {f"{r['patient']}_{r['sample']}" for r in SAMPLE_TABLE.filter(pl.col("datatype").str.contains("rna|RNA")).iter_rows(named=True)}
PATIENT_LNCRNA_SAMPLES = {f"{r['patient']}_{r['sample']}" for r in SAMPLE_TABLE.filter(pl.col("datatype").str.contains("lnc")).iter_rows(named=True)}

PATIENT_TUMOUR_SAMPLES_FROM_MATCHED = {f"{r['patient']}_{r['sample']}" for r in SAMPLE_TABLE.filter(pl.col("sampletype").str.contains("cancer|tumour|relapse|metastasis|PDX")).iter_rows(named=True) if r['patient'] in PATIENTS_WITH_GERMLINE}

FILES = {f"{r['patient']}_{r['sample']}_{r['unit']}_{r['pair']}" for r in SAMPLE_TABLE.iter_rows(named=True)}

CHROMOSOMES = [f"chr{n+1}" for n in range(22)] + [f"chr{id}" for id in ["X", "Y", "M"]]

DIFFEXP_FORMULA_CONTRASTS = config["differential_expression_contrasts"]
DIFFEXP_FORMULAE = [contrast.split('@')[0] for contrast in DIFFEXP_FORMULA_CONTRASTS]
DIFFEXP_CONTRASTS = [contrast.split('@', 1)[1] for contrast in DIFFEXP_FORMULA_CONTRASTS]
