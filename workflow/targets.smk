QC_target = "results/qc/multiqc_report.html" \
    if config["enable"]["read_QC"] or config["enable_everything"] \
       else []

coverage_target = "results/qc/coverage.tsv" \
    if config["enable"]["coverage"] or config["enable_everything"] \
       else []

rna_quant_target = expand("results/salmon/{patient_sample}_hg38/",
                                patient_sample=PATIENT_RNA_SAMPLES) \
    if config["enable"]["lncrna_quant"] or config["enable_everything"] \
       else []

lncrna_quant_target = expand("results/salmon/{patient_sample}_hg38-noncodev6-genscriptome/",
                                patient_sample=PATIENT_LNCRNA_SAMPLES) \
    if config["enable"]["lncrna_quant"] or config["enable_everything"] \
       else []

deseq_object_target = expand("results/deseq2/{formula}_{ref}.rds",
                                        formula=DIFFEXP_FORMULAE,
                                        ref="hg38-noncodev6-genscriptome") \
    if config["enable"]["differential_expression"] or config["enable_everything"] \
       else []

differential_expression_target = expand("results/deseq2/{formula_contrast}_{ref}.tsv.gz",
                                        formula_contrast=DIFFEXP_FORMULA_CONTRASTS,
                                        ref=["hg38", "hg38-noncodev6-genscriptome"]) \
    if config["enable"]["differential_expression"] or config["enable_everything"] \
       else []

immune_receptor_target = expand("results/mixcr/{patient_sample}_{ref}.vdjca",
                                patient_sample=PATIENT_SAMPLES,
                                ref=config["reference_genome"]) \
    if config["enable"]["immune_receptor"] or config["enable_everything"] \
       else []

hla_typing_target = "results/hla_typing/" \
    if config["enable"]["hla_typing"] or config["enable_everything"] \
       else []

telomere_length_target = expand("results/telomere/{patient_sample}_{ref}.csv",
                          patient_sample=PATIENT_SAMPLES,
                          ref=config["reference_genome"]) \
    if config["enable"]["telomere_length"] or config["enable_everything"] \
       else []

alignment_target = expand("results/alignment/{patient_sample}_{ref}.cram",
                          patient_sample=PATIENT_SAMPLES,
                          ref=config["reference_genome"]) \
    if config["enable"]["alignment"] or config["enable_everything"] \
       else []

pileup_target = expand("results/pileup/{patient_sample}_{ref}.pileup",
                          patient_sample=PATIENT_SAMPLES,
                          ref=config["reference_genome"]) \
    if config["enable"]["pileup"] or config["enable_everything"] \
       else []

octopus_target = expand("results/variants/octopus/annotated/{patient}_{ref}.vcf.gz",
                          patient=PATIENTS,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["SNVs"]["octopus"] or config["enable_everything"] \
       else []

mutect2_target = expand("results/variants/mutect2/annotated/{patient}_{ref}.vcf.gz",
                          patient=PATIENTS,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["SNVs"]["mutect2"] or config["enable_everything"] \
       else []

dysgu_target = expand("results/variants/dysgu/{patient_sample}_{ref}_{filt}.vcf.gz",
                          patient_sample=PATIENT_SAMPLES,
                          ref=config["reference_genome"],
                          filt=["pass", "fail"]) \
    if config["enable"]["variant_calling"]["SVs"]["dysgu"] or config["enable_everything"] \
       else []

manta_target = expand("results/variants/manta/{patient_tsample}_{ref}/",
                          patient_tsample=PATIENT_TUMOUR_SAMPLES_FROM_MATCHED,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["SVs"]["manta"] or config["enable_everything"] \
       else []

delly_target = expand("results/variants/delly/{patient}_{ref}.bcf",
                          patient=PATIENTS_WITHOUT_GERMLINE,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["SVs"]["DELLY"] or config["enable_everything"] \
       else []

delly_somatic_target = expand("results/variants/delly/filtered/{patient}_{ref}.tsv.gz",
                          patient=PATIENTS_WITH_GERMLINE,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["SVs"]["DELLY"] or config["enable_everything"] \
       else []

gridss_target = expand("results/variants/gridss/{patient}_{ref}.vcf.gz",
                          patient=PATIENTS,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["SVs"]["gridss"] or config["enable_everything"] \
       else []

sequenza_target = expand("results/variants/sequenza/{patient_tsample}_{ref}/",
                          patient_tsample=PATIENT_TUMOUR_SAMPLES_FROM_MATCHED,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["CNVs"]["sequenza"] or config["enable_everything"] \
       else []

gatk_cnv_calls_target = expand("results/variants/gatk_cnv/{patient_tsample}_{ref}_segments/{patient_tsample}.cr.called.seg",
                          patient_tsample=PATIENT_TUMOUR_SAMPLES,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["CNVs"]["GATK_CNV"] or config["enable_everything"] \
       else []

gatk_cnv_plot_target = expand("results/variants/gatk_cnv/{patient_tsample}_{ref}_segment_plots/{patient_tsample}.modeled.png",
                          patient_tsample=PATIENT_TUMOUR_SAMPLES,
                          ref=config["reference_genome"]) \
    if config["enable"]["variant_calling"]["CNVs"]["GATK_CNV"] or config["enable_everything"] \
       else []

mutect2_maf_target = expand("results/variants/mutect2/annotated/{patient_tsample}_{ref}.maf.gz",
                          patient_tsample=PATIENT_TUMOUR_SAMPLES,
                          ref=config["reference_genome"]) \
    if (config["enable"]["variant_calling"]["annotation"]["MAF"] and config["enable"]["variant_calling"]["SNVs"]["mutect2"]) or config["enable_everything"] \
       else []
