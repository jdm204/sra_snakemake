def unit_human_fastqs(wc):
    sampletype = SAMPLE_TABLE.filter(
        (pl.col("patient") == wc.patient) &
        (pl.col("sample") == wc.sample) &
        (pl.col("unit") == wc.unit) &
        (pl.col("pair") == 1)
    ).select("sampletype")\
    .to_series()[0]

    if "PDX" in sampletype and config["enable"]["xengsort"]:
        pprint(f"unit {wc.unit} of sample {wc.sample} for patient {wc.patient} has 'PDX' in sample type ({sampletype}) and enable xengsort is set in config, so filtering out mouse reads before alignment")
        basename = f"results/xengsort/{wc.patient}_{wc.sample}_{wc.unit}-k25-graft."
        return [basename + "1.fq.gz", basename + "2.fq.gz"]
    else:
        return SAMPLE_TABLE.filter(
            (pl.col("patient") == wc.patient) &
            (pl.col("sample") == wc.sample) &
            (pl.col("unit") == wc.unit)
            ).select("path").to_series().unique()

def pair_path(wc):
    return SAMPLE_TABLE.filter(
        (pl.col("patient") == wc.patient) &
        (pl.col("sample") == wc.sample) &
        (pl.col("unit") == wc.unit) &
        (pl.col("pair") == int(wc.pair))
    ).select("path").to_series().unique()

def reads_for_salmon(wc, pair):
    return SAMPLE_TABLE.filter(
        (pl.col("patient") == wc.patient) &
        (pl.col("sample") == wc.sample) &
        (pl.col("pair") == int(pair))
    ).select("path").to_series().unique()

def sample_paths(wc):
    return SAMPLE_TABLE.filter(
        (pl.col("patient") == wc.patient) &
        (pl.col("sample") == wc.sample)
    ).select("path").to_series().unique()

def sample_unit_paths(wc):
    return SAMPLE_TABLE.filter(
        (pl.col("patient") == wc.patient) &
        (pl.col("sample") == wc.sample) &
        (pl.col("unit") == wc.unit)
    ).select("path").to_series().unique()

def sample_unit_crams(wc):
    return expand("results/alignment/units/{{patient}}_{{sample}}_{unit}_{{ref}}.cram",
                  unit=SAMPLE_TABLE.filter(
                      (pl.col("patient") == wc.patient) &
                      (pl.col("sample") == wc.sample)
                  ).select("unit").to_series().unique())

def patient_sample_crams(wc):
    return expand("results/alignment/{{patient}}_{sample}_{{ref}}.cram",
           sample=SAMPLE_TABLE.filter(pl.col("patient") == wc.patient)\
                  .select("sample").to_series().unique())

def patient_tumour_crams(wc):
    return expand("results/alignment/{{patient}}_{sample}_{{ref}}.cram",
           sample=SAMPLE_TABLE.filter(
               (pl.col("sampletype") != "normal") &
               (pl.col("patient") == wc.patient))\
                  .select("sample").to_series().unique())

# TODO: handle multiple normals gracefully (maybe merge)
def patient_normal_crams(wc):
    return expand("results/alignment/{{patient}}_{sample}_{{ref}}.cram",
           sample=SAMPLE_TABLE.filter(
               (pl.col("sampletype") == "normal") &
               (pl.col("patient") == wc.patient))\
                  .select("sample").to_series().unique())

def patient_normal_samples(wc):
    return SAMPLE_TABLE.filter(
               (pl.col("sampletype") == "normal") &
               (pl.col("patient") == wc.patient))\
               .select("sample").to_series().unique()

def patient_tumour_samples(wc):
    return SAMPLE_TABLE.filter(
               (pl.col("sampletype") != "normal") &
               (pl.col("patient") == wc.patient))\
               .select("sample").to_series().unique()

